import React from 'react';
import ReactDOMClient  from 'react-dom/client';

import '../../../dev-tools/project-sass/main.scss';

import Homepage from './views/Homepage';
import UserDirectory from './views/UserDirectory';

const container = document.getElementById("root");
const root = ReactDOMClient.createRoot(container);

root.render(
  <React.StrictMode>
      {/* <Homepage /> */}
      <UserDirectory />
  </React.StrictMode>
);
