import React from 'react';

type RecipeType = {
  id: number;
  title: string;
  image: string;
  imageType: 'png' | 'jpg';
}

type RecipeResultsProps = {
  recipes: RecipeType[];
}

/**
 * Component to render an array of recipes
 * @param props an array of recipes
 * @returns 
 */
const RecipeResults = (props: RecipeResultsProps) => {
  const { recipes } = props;

  return (
    <div className="recipe-card-wrapper">
      {
        recipes.map((recipe: RecipeType, index: number) => {
          return <div className="recipe-card" key={recipe.id}>
            <img width="300" src={recipe.image} alt="" />
            <h4>{recipe.title}</h4>
            Recipe {index + 1}
          </div>
        })
      }
    </div>
  )
};

export default RecipeResults;