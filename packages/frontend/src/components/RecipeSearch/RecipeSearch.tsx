import React, { useState } from 'react';

type RecipeSearchProps = {
  handleSubmit: any
};

/**
 * Simple search component for recipes
 * @props
 * @returns 
 */
const RecipeSearch = (props: RecipeSearchProps) => {
  const { handleSubmit } = props;
  const [value, setValue] = useState('');

  /**
   * could be useful to update the redux store - if/when it's added (prevent searching the same term)
   * @param event 
   */
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();
    setValue(event.target.value);
  };

  return (
    <div className="recipe-search-wrapper">
      <form className="recipe-search-form" onSubmit={handleSubmit}>
        <input className="recipe-search-input" name="recipe" onChange={handleChange} type="input" placeholder="Search..."></input>
      </form>
    </div>
  )
}

export default RecipeSearch;
