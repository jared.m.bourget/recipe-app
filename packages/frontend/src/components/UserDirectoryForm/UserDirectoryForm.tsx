import React, { useState, useCallback } from 'react';
import axios from 'axios';

/** @TODO create a global user type */
type UserType = {
  firstName: string;
  lastName: string;
  gender: 'male' | 'female' | 'other' | string;
}

type UserDirectoryFormProps = {
  handleSave?: (user: UserType) => void;
  handleCancel?: () => void;
  user?: UserType;
  randomUser?: UserType;
}

const UserDirectoryForm = (props: UserDirectoryFormProps) => {
  const {
    handleSave,
    handleCancel,
  } = props;

  const [userForm, setUserForm] = useState({
    firstName: "",
    lastName: "",
    gender: "",
  });

  const handleFetchRandomUser = useCallback(async () => {
    try {
      const res = await axios.get('https://randomuser.me/api');
      const { data } = res;
      const fetchedUser = {
        firstName: data.results[0].name.first,
        lastName: data.results[0].name.last,
        gender: data.results[0].gender
      };
      setUserForm(fetchedUser);

    } catch (error) {
      console.log(error)
    }
  }, []);

  const handleGenderSelect = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    setUserForm(
      {
        ...userForm,
        gender: value,
      }
    )
  }

  const handleNameInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    setUserForm(
      {
        ...userForm,
        [event.target.id]: value,
      }
    )
  }

  const handleNewUserSave = (event: React.MouseEvent) => {
    event.preventDefault();
    handleSave(userForm);
  }

  return (
    <div className="user-directory-form-wrapper">
      <form className="user-directory-form">
        <label>First
          <input id="firstName" onChange={handleNameInput} type="text" placeholder="" value={userForm.firstName}></input>
        </label>
        <label>Last
          <input id="lastName" onChange={handleNameInput} type="text" placeholder="" value={userForm.lastName}></input>
        </label>
        <div className="radio-select-wrapper">
          Gender:
          <label className="radio-select">
            Male
            <input onChange={handleGenderSelect} checked={userForm.gender === "male"} type="radio" value="male"></input>
          </label>
          <label className="radio-select">
            Female
            <input onChange={handleGenderSelect} checked={userForm.gender === "female"} type="radio" value="female"></input>
          </label>
          <label className="radio-select">
            Other
            <input onChange={handleGenderSelect} checked={userForm.gender === "other"} type="radio" value="other"></input>
          </label>
        </div>
      </form>
      <div className="form-actions-wrapper">
        <button className="button-random" type="button" onClick={handleFetchRandomUser}>Random User</button>
        <button className="button-save" type="button" onClick={handleNewUserSave}>Save</button>
        <button className="button-cancel" type="button" onClick={handleCancel}>Cancel</button>
      </div>
    </div>
  )
}

export default UserDirectoryForm;
