import React from 'react';

type UserDirectoryFormProps = {
  handleToggleAddUser?: () => void;
}

const UserDirectoryHeader = (props: UserDirectoryFormProps) => {
  const { handleToggleAddUser } = props;
  return (
    <nav className="user-directory-nav">
      <div>
        <span>User Directory</span>
      </div>
      <div>
        <button type="button" onClick={handleToggleAddUser} >Add User</button>
      </div>
    </nav>
  )
}

export default UserDirectoryHeader;
