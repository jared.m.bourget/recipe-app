import React from 'react';

type UsersDirectoryListProps = {
  firstName: string;
  lastName: string;
  gender: 'Male' | 'Female' | 'Other';
}

/**
 * Component to render a list of users, one user at a time
 * @param props 
 * @returns 
 */
const UsersDirectoryList = (props: UsersDirectoryListProps) => {
  const {
    firstName,
    lastName,
    gender,
  } = props;

  return (
    <li className="user-directory-list-item">
      <div>
        First: <span>{firstName}</span>
      </div>
      <div>
        Last: <span>{lastName}</span>
      </div>
      <div>
        Gender: <span>{gender}</span>
      </div>
    </li>
  )
}

export default UsersDirectoryList;
