import React, { useState, useCallback } from 'react';

import UserDirectoryForm from '../../components/UserDirectoryForm';
import UserDirectoryHeader from '../../components/UserDirectoryHeader';
import UserDirectoryList from '../../components/UserDirectoryList';

type UserType = {
  firstName: string;
  lastName: string;
  gender: 'Male' | 'Female' | 'Other';
}

/**
 * 
 * @returns 
 */
const UserDirectory = () => {
  const [toggleAddUser, setToggleAddUser] = useState(true);
  const [users, setUsers] = useState([{
    firstName: "John",
    lastName: "Snow",
    gender: "male"
  }]);

  const handleToggleAddUser = useCallback(() => {
    setToggleAddUser((prevState) => !prevState);
  }, []);

  const handleSave = useCallback((newUser: UserType) => {
    setUsers((prevState) => [...prevState, newUser]);
  }, []);

  return (
    <div>
      <UserDirectoryHeader handleToggleAddUser={handleToggleAddUser} />
      <ul className="user-directory-list">
        {
          users.length && (
            users.map((user: UserType, index) => (
              <UserDirectoryList key={index} {...user} />
            ))
          )
        }
      </ul>
      {
        toggleAddUser && (
          <UserDirectoryForm handleSave={handleSave} handleCancel={handleToggleAddUser} />
        )
      }
    </div>
  )
}

export default UserDirectory;
