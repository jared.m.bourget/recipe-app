import React, { useState, useCallback } from 'react';
import RecipeSearch from '../../components/RecipeSearch';
import RecipeResults from '../../components/RecipeResults';
import axios from 'axios';

/** Data to initialize app with - could use "useEffect" to make an initial call, but I only have 150 requests per day */
const testData = [
  { id: 641717, title: 'Duck Rumaki', image: 'https://spoonacular.com/recipeImages/641717-312x231.jpg', imageType: 'jpg' },
  { id: 641697, title: 'Duck Breast with Redcurrant and Port Sauce', image: 'https://spoonacular.com/recipeImages/641697-312x231.jpg', imageType: 'jpg' },
  { id: 641700, title: 'Duck Egg Omelette With Caviar and Sour Cream', image: 'https://spoonacular.com/recipeImages/641700-312x231.jpg', imageType: 'jpg' },
  { id: 641720, title: 'Duck with Dried Cranberries, Tangerines and Mascarpone Sauce', image: 'https://spoonacular.com/recipeImages/641720-312x231.jpg', imageType: 'jpg' },
  { id: 664960, title: 'Warm Duck Salad With Roasted Beetroot', image: 'https://spoonacular.com/recipeImages/664960-312x231.jpg', imageType: 'jpg' },
  { id: 645694, title: 'Grilled Duck', image: 'https://spoonacular.com/recipeImages/645694-312x231.jpg', imageType: 'jpg' },
  { id: 654468, title: 'Pan-Seared Duck With Blueberry Glaze', image: 'https://spoonacular.com/recipeImages/654468-312x231.jpg', imageType: 'jpg' },
  { id: 660312, title: 'Slow-roasted duck legs', image: 'https://spoonacular.com/recipeImages/660312-312x231.jpg', imageType: 'jpg' },
  { id: 643016, title: 'Five Spice & Orange Duck', image: 'https://spoonacular.com/recipeImages/643016-312x231.jpg', imageType: 'jpg' },
  { id: 637276, title: 'Cassoulet with Chicken or Duck', image: 'https://spoonacular.com/recipeImages/637276-312x231.jpg', imageType: 'jpg' },
]

/**
 * Homepage component - combines the recipe search component and the results page
 * @returns 
 */
const Homepage = () => {
  /** @TODO - add spinner and empty results page */
  const [isLoading, setIsLoading] = useState(false);
  const [currentRecipes, setCurrentRecipes] = useState([]);

  /**
   * Function to fetch recipes by keyword
   */
  const handleFormSubmit = useCallback(async (event: React.FormEvent) => {
    event.preventDefault();

    // @ts-ignore - @TODO update type
    const element = event.target.firstChild as HTMLInputElement;
    const value = element.value;
    setIsLoading(true);
    const API_KEY = process.env.REACT_APP_API_KEY;

    try {
      const res = await axios.get(`https://api.spoonacular.com/recipes/complexSearch?query=${value}&apiKey=${API_KEY}`, {});

      const { data } = res;
      const recipes = data.results || [];
      setIsLoading(false)

      /** @TODO - remove "any" */
      setCurrentRecipes(recipes as any);
    } catch (error) {
      setIsLoading(false)
      console.log(error);
    }

  }, [setIsLoading, setCurrentRecipes]);

  return (
    <div className="homepage-container">
      <RecipeSearch handleSubmit={handleFormSubmit} />
      <RecipeResults recipes={currentRecipes.length ? currentRecipes : testData as any} />
    </div>
  )
}

export default Homepage;
