# Simple Recipe Search Application



## Description
This is a quick project to demonstrate a simple project scaffolding using `npm workspaces`, `React`, `TypeScript`, and `SASS`

## Starting the Application
- run `npm install`
- run `npm run build` - this will build the packages in the workspace
- cd into `packages/frontend`
- An api key will needed from [Spoonacular](https://spoonacular.com/)
  and added to a `.env` file in the package `REACT_APP_API_KEY=spoonacularKey`
- run `npm run start` and open `http://localhost:8080/`
- search for a recipe keyword
